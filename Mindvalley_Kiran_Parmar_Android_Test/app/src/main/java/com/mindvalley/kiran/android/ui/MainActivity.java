package com.mindvalley.kiran.android.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mindvalley.kiran.android.R;
import com.mindvalley.kiran.android.library.network.BitmapDownloadTask;
import com.mindvalley.kiran.android.library.network.NetworkHandler;
import com.mindvalley.kiran.android.library.network.NetworkTaskCallback;
import com.mindvalley.kiran.android.library.network.parser.DownloaderAndParserTask;
import com.mindvalley.kiran.android.library.network.parser.DownloaderAndParserTask.RemoteDataType;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity implements NetworkTaskCallback {
	
	/** The URL to fetch the initial data from */
	private static final String SERVER_URL = "http://pastebin.com/raw/wgkJgazE";

	private ListView mListView;
	private SwipeRefreshLayout mSwipeRefreshLayout;

	/** NetworkHandler instance from library */
	private NetworkHandler mNetworkHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mListView = (ListView) findViewById(R.id.list);

		mNetworkHandler = new NetworkHandler(this);
		
		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
		mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh() {
				// Code to refresh the list
				loadData();
			}
		});

		mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		mSwipeRefreshLayout.post(new Runnable() {
			@Override
			public void run() {
				loadData();				
			}
		});
	}

	private void loadData() {
		mSwipeRefreshLayout.setRefreshing(true);
		mNetworkHandler.queueRequest(DownloaderAndParserTask.getTaskInstanceFor(SERVER_URL, this, null, RemoteDataType.JSON));
	}

	@Override
	public void onLoadCompleted(Object loadedData, View view, String loadedURL) {
		mSwipeRefreshLayout.setRefreshing(false);
		if (loadedData != null) {
			CustomListAdapter adapter = new CustomListAdapter(this, (List<HashMap<String, String>>) loadedData);
			mListView.setAdapter(adapter);
		}
	}

	@Override
	protected void onStop() {
		mNetworkHandler.shutDown();
		super.onStop();
	}

	/**
	 * The adapter for ListView
	 */
	private class CustomListAdapter extends BaseAdapter {

		/**
		 * Parsed-data list
		 */
		private List<HashMap<String, String>> parsedList;
		private Context context;

		public CustomListAdapter(Context context, List<HashMap<String, String>> parsedList) {
			this.parsedList = parsedList;
			this.context = context;
		}

		@Override
		public int getCount() {
			return parsedList.size();
		}

		@Override
		public Object getItem(int position) {
			return parsedList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View returnView = null;
			ViewHolder holder;

			if (convertView != null) {
				returnView = convertView;
				holder = (ViewHolder) returnView.getTag();
			} else {
				returnView = LayoutInflater.from(context).inflate(R.layout.list_item, null);
				holder = new ViewHolder();
				holder.imageView = (ImageView) returnView.findViewById(R.id.imageView);
				holder.textView = (TextView) returnView.findViewById(R.id.username);
				returnView.setTag(holder);
			}

			HashMap<String, String> data = parsedList.get(position);

			String imageURL = data.get("urls");
			String name = data.get("name");

			holder.id = (int) getItemId(position);
			holder.imageURL = imageURL;
			holder.textView.setText(TextUtils.isEmpty(name) ? "" : name);
			holder.imageView.setImageDrawable(null);

			mNetworkHandler.queueRequest(new BitmapDownloadTask(imageURL, new NetworkTaskCallback() {
				@Override
				public void onLoadCompleted(Object loadedData, View view, String loadedURL) {
					if (view.isShown() && loadedData != null) {
						ViewHolder holder = (ViewHolder) view.getTag();
						if (holder.imageURL.equals(loadedURL)) {
							holder.imageView.setImageBitmap((Bitmap) loadedData);
						}
					}
				}
			}, returnView));

			return returnView;
		}
	}
}
