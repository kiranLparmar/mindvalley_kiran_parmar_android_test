package com.mindvalley.kiran.android.ui;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * ViewHolder class for holding data for ListView items
 */
public class ViewHolder {

	/**
	 * View for showing user's image
	 */
	public ImageView imageView;
	
	/**
	 * TextView for showing user's name
	 */
	public TextView textView;
	
	/**
	 * ID of the item represented by this ViewHolder in ListView
	 */
	public int id;
	
	/**
	 * URL of the associated image that would be shown in imageView
	 */
	public String imageURL;
}
