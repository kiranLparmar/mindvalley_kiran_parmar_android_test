package com.mindvalley.kiran.android.library.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.view.View;

/**
 * NetworkTask extension to download File(String) data.
 * This can be saved to file(s) by processing the data in NetworkTaskCallback
 */
public class FileDownloadTask extends NetworkTask {

	public FileDownloadTask(String url, NetworkTaskCallback callback, View view) {
		super(url, callback, view);
	}

	@Override
	public Object processStream(InputStream inputStream) {
		try {
			BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
			StringBuilder total = new StringBuilder(inputStream.available());
			String line;
			while ((line = r.readLine()) != null) {
				total.append(line).append('\n');
			}
			return total.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
