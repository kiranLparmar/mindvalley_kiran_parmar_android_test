package com.mindvalley.kiran.android.library.network;

import android.view.View;

/**
 * Callback to be invoked once the related NetworkTask finishes execution. Will
 * be run on UI-thread with the downloaded data (maybe null)
 */
public interface NetworkTaskCallback {

	/**
	 * Will be called on UI thread with the downloaded-data Object, the view
	 * supplied to the DownloadTask & the URL loaded by the task calling this
	 * method
	 * 
	 * @param loadedData
	 * @param view
	 * @param loadedURL
	 */
	public void onLoadCompleted(Object loadedData, View view, String loadedURL);
}
