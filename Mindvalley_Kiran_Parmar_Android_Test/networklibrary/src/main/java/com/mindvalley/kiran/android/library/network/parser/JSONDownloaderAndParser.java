package com.mindvalley.kiran.android.library.network.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mindvalley.kiran.android.library.network.NetworkTaskCallback;

import android.view.View;

/**
 * Extension of DownloaderAndParserTask for JSON data
 */
public class JSONDownloaderAndParser extends DownloaderAndParserTask {

	private List<HashMap<String, String>> parsedList;

	public JSONDownloaderAndParser(String url, NetworkTaskCallback callback, View view) {
		super(url, callback, view);
	}

	@Override
	public Object processStream(InputStream inputStream) {
		return parseData((String) super.processStream(inputStream));
	}

	@Override
	public Object parseData(String downloadedData) {
		try {
			JSONArray jsonArr = new JSONArray(downloadedData);
			parsedList = parseArrayToList(jsonArr);
			return parsedList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Does the JSON parsing. Currently the code is made specific to the JSON
	 * data on URL: "http://pastebin.com/raw/wgkJgazE", but can be made generic
	 * to return a List of HashMap of the keys and values from JSON with keys as String
	 * & the values as either String or another List of HashMaps of same type
	 * 
	 * @param jsonArr
	 * @return
	 */
	private List<HashMap<String, String>> parseArrayToList(JSONArray jsonArr) {
		List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		try {
			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonItem = jsonArr.getJSONObject(i);
				JSONObject jsonItemUserDetails = jsonItem.getJSONObject("user");
				JSONObject jsonItemUserURLs = jsonItem.getJSONObject("urls");

				HashMap<String, String> listItem = new HashMap<String, String>();

				listItem.put("name", jsonItemUserDetails.getString("name"));
				listItem.put("profile_image", jsonItemUserDetails.getJSONObject("profile_image").getString("medium"));
				listItem.put("urls", jsonItemUserURLs.getString("regular"));

				list.add(listItem);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
