package com.mindvalley.kiran.android.library.network.parser;

/**
 * Parser interface
 */
public interface Parser {

	/**
	 * Implementations parse the given data & return an appropriate Object, generally a List implementation
	 * @param downloadedData
	 * @return
	 */
	public Object parseData(String downloadedData);
}
