package com.mindvalley.kiran.android.library.utility;

import java.util.LinkedHashMap;

/**
 * Singleton class to manage the caching of data, based on the mechanism of
 * Least-recently-used data being the first to be evicted from the cache.
 * Cache capacity can be set by calling setMaxCapacity.
 */
public class CacheManager {

	// XXX: Can also be implemented using LruCache provided by Android. But I've used my custom LinkedHashMap implementation
	
	/**
	 * Singleton instance
	 */
	private static final CacheManager sInstance = new CacheManager();

	/**
	 * Default/minimum capacity of the cache
	 */
	private static final short DEFAULT_CAPACITY = 12;
	
	private CacheMap<String, Object> mCacheMap;
	
	private CacheManager() {
		// Singleton
	}
	
	/**
	 * Returns the CacheManager singleton instance
	 * @return
	 */
	public static CacheManager get() {
		if (sInstance.mCacheMap == null) {
			sInstance.init();
		}
		return sInstance;
	}
	
	private void init() {
		mCacheMap = new CacheMap<String, Object>(DEFAULT_CAPACITY);
	}
	
	/**
	 * Empties all the contents from cache
	 */
	public void clearCache() {
		if (mCacheMap != null) {
			mCacheMap.clear();
		}
	}

	/**
	 * Sets the maximum number of cached entries allowed (minimum 12)
	 * @param capacity
	 */
	public void setMaxCapacity(int capacity) {
		if (capacity < DEFAULT_CAPACITY) {
			capacity = DEFAULT_CAPACITY;
		}
		
		if (mCacheMap.getCapacity() == capacity) {
			// Already same size...return
			return;
		}
		
		CacheMap<String, Object> map = new CacheMap<String, Object>(capacity);
		map.putAll(mCacheMap);
		clearCache();
		mCacheMap = map;
	}
	
	/**
	 * Returns the cached data for provided key
	 * @param key
	 * @return
	 */
	public Object getCachedData(String key) {
		return mCacheMap.get(key);
	}

	/**
	 * Adds provided key/Object data to cache
	 * @param key
	 * @param value
	 */
	public void addToCache(String key, Object value) {
		if (mCacheMap == null) {
			init();
		}
		mCacheMap.put(key, (value));
	}
	
	private class CacheMap<K, V> extends LinkedHashMap<K, V> {

		private static final long serialVersionUID = 1L;
		
		private int mCapacity;
		
		public CacheMap(int capacity) {
			// Instantiate with the provided capacity, default load-factor of
			// 0.75f & set accessOrder mode to true to allow modifying the Map
			// based on access order of elements
			super(capacity, 0.75f, true);
			mCapacity = capacity;
		}
		
		@Override
		protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest) {
			if (size() >= mCapacity) {
				// return true so that the 'eldest' (least recently used) element is removed from the map
				return true;
			}
			return false;
		}
		
		/**
		 * Returns the current maximum capacity
		 * @return
		 */
		public int getCapacity() {
			return mCapacity;
		}
	}

}
