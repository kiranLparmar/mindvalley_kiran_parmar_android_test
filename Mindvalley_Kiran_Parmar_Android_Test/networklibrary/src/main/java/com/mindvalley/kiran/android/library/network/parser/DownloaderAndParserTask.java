package com.mindvalley.kiran.android.library.network.parser;

import com.mindvalley.kiran.android.library.network.NetworkTaskCallback;
import com.mindvalley.kiran.android.library.network.FileDownloadTask;

import android.view.View;

/**
 * An abstract class extending the FileDownloadTask & providing additional
 * capabilities to allow parsing the data, be it XML OR JSON. More types can be
 * added, if required, by adding necessary implementation of Parser & adding it
 * to the supported RemoteDataType enum.
 * 
 * Applications can directly use the static getTaskInstanceFor() method to get
 * appropriate DownloaderAndParserTask subclass instance that is suitable to
 * specified RemoteDataType
 */
public abstract class DownloaderAndParserTask extends FileDownloadTask implements Parser {

	public enum RemoteDataType {
		XML,
		JSON
	}

	public DownloaderAndParserTask(String url, NetworkTaskCallback callback, View view) {
		super(url, callback, view);
	}

	/**
	 * Returns a DownloaderAndParserTask implementation depending on whether
	 * RemoteDataType is XML or JSON. Defaults to JSON
	 * 
	 * @param url
	 * @param callback
	 * @param view
	 * @param type
	 * @return
	 */
	public static DownloaderAndParserTask getTaskInstanceFor(String url, NetworkTaskCallback callback, View view, RemoteDataType type) {
		switch(type) {
			case XML:
				return new XMLDownloaderAndParser(url, callback, view);
			case JSON:
			default:
				return new JSONDownloaderAndParser(url, callback, view);
		}
	}
}
