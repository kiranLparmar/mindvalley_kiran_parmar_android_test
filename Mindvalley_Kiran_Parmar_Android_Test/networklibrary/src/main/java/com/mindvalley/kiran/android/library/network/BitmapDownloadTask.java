package com.mindvalley.kiran.android.library.network;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

/**
 * NetworkClass extension to support Downloading Bitmaps
 */
public class BitmapDownloadTask extends NetworkTask {

	public BitmapDownloadTask(String url, NetworkTaskCallback callback, View view) {
		super(url, callback, view);
	}

	@Override
	public Bitmap processStream(InputStream inputStream) {
		final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
		return bitmap;
	}

}
