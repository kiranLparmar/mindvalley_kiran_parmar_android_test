package com.mindvalley.kiran.android.library.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

public class Utils {
	
	private static final String TAG = "Utils";

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connected = (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable()
				&& connectivityManager.getActiveNetworkInfo().isConnected());
		if (!connected) {
			Log.e(TAG, "Network Unavailable");
			return false;
		}
		return true;
	}

}
