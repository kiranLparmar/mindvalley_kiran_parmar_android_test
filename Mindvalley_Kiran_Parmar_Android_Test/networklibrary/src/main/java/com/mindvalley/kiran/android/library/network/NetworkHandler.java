package com.mindvalley.kiran.android.library.network;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.mindvalley.kiran.android.library.utility.CacheManager;
import com.mindvalley.kiran.android.library.utility.Utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

/**
 * NetworkHandler class provides a Fixed-thread-pool ExecutorService instance to allow applications to queue NetworkTask instances.
 * The queued requests return a Future instance which can be used to cancel the requests.
 * This also contains mechanism to cache the downloaded data, irrespective of the type of retrieved data.
 */
public class NetworkHandler {

	private Context mContext;
	
	/**
	 * Handler created on UI-thread
	 */
	private Handler mUIHandler;

	/**
	 * Fixed Thread Pool ExecutorService
	 */
	private ExecutorService executorService;

	/**
	 * Set containing list of URLs that are currently being requested & queued/running, to prevent adding them again.
	 */
	private Set<String> serverRequestsSent = Collections.synchronizedSet(new HashSet<String>());

	/**
	 * Cache manager instance
	 */
	private CacheManager mCacheManager;

	public NetworkHandler(Context context) {
		mContext = context;
		
		mUIHandler = new Handler(Looper.getMainLooper());
		
		int availableProcessors = Runtime.getRuntime().availableProcessors();
		executorService = Executors.newFixedThreadPool(availableProcessors > 2 ? availableProcessors : 2);
		
		mCacheManager = CacheManager.get();
	}

	/**
	 * Queues the NetworkTask request and returns a Future instance,
	 * if queued, to allow calling Thread to cancel it
	 * 
	 * @param request
	 * @return
	 */
	public Future queueRequest(final NetworkTask request) {
		String url = request.getURL();
		
		// FIXME: Need to take care of the fact that same resource may be
		// requested by multiple sources simultaneously. So ignoring them would
		// also cause their respective NetworkTaskCallback to be ignored as well. Need to
		// have a HashMap for each running requests containing list of the callbacks
		// to be invoked on completion.
		if (!serverRequestsSent.contains(url)) {
			// Only queue the request if it has not been queued before...and
			final Object cachedData = mCacheManager.getCachedData(url);
			if (cachedData == null) {
				//...and there is no cached data for it either
				serverRequestsSent.add(url);
				return executorService.submit(new RunnableTask(request, mUIHandler));
			} else {
				// In case of cached data available, simply make the onLoadCompleted callback to be run on mUIHandler
				mUIHandler.post(new Runnable() {
					@Override
					public void run() {
						NetworkTaskCallback callback = request.getCallback();
						if (callback != null) {
							callback.onLoadCompleted(cachedData, request.getView(), request.getURL());
						}
					}
				});
			}
		}
		return null;
	}

	/**
	 * Shuts down the Executor service
	 */
	public void shutDown() {
		executorService.shutdownNow();
	}

	/**
	 * Runnable wrapper around the NetworkTask instances, used when adding the requests to queue
	 */
	private class RunnableTask implements Runnable {
		private NetworkTask request;
		private Handler handler;

		public RunnableTask(NetworkTask request, Handler handler) {
			this.request = request;
			this.handler = handler;
		}

		@Override
		public void run() {
			try {
				if (Utils.isNetworkAvailable(mContext)) {
					Object object = request.execute(handler);
					// remove from Set on completion of request
					serverRequestsSent.remove(request.getURL());
					// add fetched data to cache
					if (object != null) {
						mCacheManager.addToCache(request.getURL(), object);
					}
				} else {
					Log.e("NetworkHandler", "Network unavailable!");
					serverRequestsSent.remove(request.getURL());
					handler.post(new Runnable() {
						
						@Override
						public void run() {
							Toast.makeText(mContext, "Network unavailable!", Toast.LENGTH_SHORT).show();
							NetworkTaskCallback callback = request.getCallback();
							if (callback != null) {
								callback.onLoadCompleted(null, request.getView(), request.getURL());
							}
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
