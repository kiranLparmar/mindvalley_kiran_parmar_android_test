package com.mindvalley.kiran.android.library.network.parser;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import com.mindvalley.kiran.android.library.network.NetworkTaskCallback;

import android.view.View;

/**
 * Extension of DownloaderAndParserTask for XML data. This is just a stub-class
 * & does no real parsing, created as an example to show how easy it is to add a
 * new DataType Parser to this library
 */
public class XMLDownloaderAndParser extends DownloaderAndParserTask {

	private List<HashMap<String, String>> parsedList;

	public XMLDownloaderAndParser(String url, NetworkTaskCallback callback, View view) {
		super(url, callback, view);
	}

	@Override
	public Object processStream(InputStream inputStream) {
		return parseData((String) super.processStream(inputStream));
	}

	@Override
	public Object parseData(String downloadedData) {
		// TODO: Add XML parsing code to construct similar structure as returned by the JSON-parser in JSONDownloaderAndParser
		return parsedList;
	}

}
