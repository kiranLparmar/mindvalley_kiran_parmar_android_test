package com.mindvalley.kiran.android.library.network;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

/**
 * An abstract class having methods to download data from provided URL. The
 * processing of data into required formats & parsing is handled by
 * implementations extending this class.
 */
public abstract class NetworkTask {

	private static final String TAG = "DownloadTask";

	/**
	 * The URL from which data is to be fetched
	 */
	private String mURL;

	/**
	 * Callback instance supplied which will be invoked on UI-thread once the
	 * processing is complete
	 */
	private NetworkTaskCallback mCallback;

	/**
	 * The View associated with this task. Useful for cases such as an ImageView
	 * requesting a bitmap-download task OR a TextView requesting some
	 * String-data to be fetched, etc.
	 */
	private View view;

	/**
	 * 
	 * @param url
	 *            The URL from which data is to be fetched
	 * @param callback
	 *            NetworkTaskCallback instance supplied which will be invoked on
	 *            UI-thread once the processing is complete
	 * @param view
	 *            The View associated with this task. Useful for cases such as
	 *            an ImageView requesting a bitmap-download task OR a TextView
	 *            requesting some String-data to be fetched, etc.
	 */
	public NetworkTask(String url, NetworkTaskCallback callback, View view) {
		mURL = url;
		mCallback = callback;
		this.view = view;
	}

	/**
	 * Returns the URL from which data is to be fetched
	 * 
	 * @return
	 */
	public String getURL() {
		return mURL;
	}

	/**
	 * Returns the View associated with this Task
	 * @return
	 */
	public View getView() {
		return view;
	}

	/**
	 * Returns the NetworkTaskCallback instance supplied to this task
	 * 
	 * @return
	 */
	public NetworkTaskCallback getCallback() {
		return mCallback;
	}

	/**
	 * Executes this task, involving connecting to the URL & fetching the
	 * content's stream which is then processed by the sub-class implementations
	 * of processStream()
	 * 
	 * @param handler
	 * @return
	 */
	public Object execute(Handler handler) {
		if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
			throw new RuntimeException("cannot be executed on main/UI-thread");
		}

		final Object object = downloadData();

		handler.post(new Runnable() {
			@Override
			public void run() {
				if (mCallback != null) {
					mCallback.onLoadCompleted(object, view, mURL);
				}
			}
		});

		return object;
	}

	/**
	 * Given a URL, establishes an HttpUrlConnection and retrieves the
	 * content as a InputStream, which it passes on to the processStream()
	 * method
	 */
	private Object downloadData() {
		InputStream is = null;

		try {
			URL url = new URL(mURL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();
			
			int response = conn.getResponseCode();
			
			Log.d(TAG, "Response code: " + response + "for URL:" + mURL);
			
			is = conn.getInputStream();
			return processStream(is);

		} catch (IOException e) {
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// Ignore
				}
			}
		}
	}

	/**
	 * Method to allow subclasses to determine the operation to be performed on
	 * the stream. Subclasses do not need to worry about closing the stream.
	 * 
	 * @param inputStream
	 * @return
	 */
	public abstract Object processStream(InputStream inputStream);

}
